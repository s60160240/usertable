
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class userTablemodel extends AbstractTableModel{
    String[] columnName = {"id","username","name","surname"};
    ArrayList<user> list = data.list;
    public userTablemodel(){
        
    }
    public String getColumnName(int column){
        return columnName[column];    
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;   
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        user us= list.get(rowIndex);
        if(us == null)return "";
        switch(columnIndex){
            case 0:return us.getId();
            case 1:return us.getLogin();
            case 2:return us.getName();
            case 3:return us.getSurname();
        }
        return "";
    }
    
}
